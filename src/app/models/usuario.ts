export class Usuario {
    cedula: string;
    nombre: string;
    apellido: string;
    direccion: string;
    email: string;
    clave: string;
    ncuenta:string;
    saldo:number;

    setClave(clave: string) {
        this.clave = clave;
    }
    getClave() {
        return this.clave;
    }
}