import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { RecuperCuentaService } from 'src/app/service/recuper-cuenta.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Usuario } from 'src/app/models/usuario';
import { Credito } from 'src/app/models/credito';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Pago } from 'src/app/models/pago';
@Component({
  selector: 'app-transaccion',
  templateUrl: './transaccion.page.html',
  styleUrls: ['./transaccion.page.scss'],
})
export class TransaccionPage implements OnInit {
  link: string = "../../../assets/icon/user.png";
  public usuario:Usuario;
  public ncuenta:string;
  public credito:Credito;
  public creditos: Array<Credito>;
  public pagos: Array<Pago>;
  constructor(private _route: ActivatedRoute, private _service: RecuperCuentaService,private router: Router) {
    this.usuario = new Usuario();
    this._route.params.subscribe(param => {
      console.log('Consulta');
      this._service.recuperar(param['ncuenta']).then(res => {
        this.usuario.ncuenta = res['numeroCuenta'];
        this.usuario.saldo = res['saldoCuenta'];
        let user = res['usuario'];
        this.usuario.nombre = user['nombre'];
        this.usuario.apellido=user['apellido'];
      });
    });

    this._route.params.subscribe(param => {
      this.creditos = new Array<Credito>();
     this.creditos= this._service.recuperarCreditos(param['ncuenta']);
     console.log(this.creditos);
    });

    this._route.params.subscribe(param => {
      this.pagos = new Array<Pago>();
     this.pagos= this._service.recuperarPagos(param['ncuenta']);
     console.log(this.pagos);
    });
  }

  ngOnInit() {
  }
pasarpag(){
  this._route.params.subscribe(param => {
  this.router.navigate(['/realizar',param['ncuenta']])
});
}

}
