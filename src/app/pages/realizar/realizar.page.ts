import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TransaccionService } from 'src/app/service/transaccion.service';

@Component({
  selector: 'app-realizar',
  templateUrl: './realizar.page.html',
  styleUrls: ['./realizar.page.scss'],
})
export class RealizarPage implements OnInit {

  constructor(private _route: ActivatedRoute, private _service:TransaccionService,private router: Router) { }

  ngOnInit() {
  }

  realizarPago(cuentadestino,monto){
    
   this._route.params.subscribe(param => {
     const cuentaorigen=param['ncuenta']
    console.log(cuentaorigen,cuentadestino,monto)
      this._service.realizado(cuentaorigen,cuentadestino,monto)
   });
  }
  Regresar(){
    this._route.params.subscribe(param => {
    this.router.navigate(['/transaccion',param['ncuenta']])
  });
  }

}
