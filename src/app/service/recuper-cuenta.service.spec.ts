import { TestBed } from '@angular/core/testing';

import { RecuperCuentaService } from './recuper-cuenta.service';

describe('RecuperCuentaService', () => {
  let service: RecuperCuentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecuperCuentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
