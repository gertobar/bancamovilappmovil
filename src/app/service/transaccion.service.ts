import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
const URL = "http://localhost:8080/ProyectoAppDisServer/ws/cuenta/trx";
@Injectable({
  providedIn: 'root'
})
export class TransaccionService {

  constructor(private http: HttpClient, private router: Router) { }
  realizado(cuentaorigen: String ,cuentadestino: String, monto:number) {
    const tipo="tranferencia"
    var trx = { cuentaorigen: cuentaorigen,cuentadestino: cuentadestino, monto: monto,tipo:tipo};
    return new Promise(resolve => {
      this.http.post(URL, trx).subscribe(data => {
        console.log(data);
        this.router.navigate(['/transaccion',cuentaorigen])
      }, err => {
        console.log(err);
      });
    });

  }
}
