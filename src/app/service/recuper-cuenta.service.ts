import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Credito } from '../models/credito';
import { Pago } from '../models/pago';

const URL = "http://localhost:8080/ProyectoAppDisServer/ws/InfoCuenta?numeroCuenta";

@Injectable({
  providedIn: 'root'
})
export class RecuperCuentaService {
  static URL2 = "http://localhost:8080/ProyectoAppDisServer/ws/ListaCredito?numeroCuenta";
  static URL3 = "http://localhost:8080/ProyectoAppDisServer/ws/ListaPagos?codigo_pre";
  private creditos: Array<Credito>;
  private pagos: Array<Pago>;
  constructor(private http: HttpClient) { }
  recuperar(ncuenta: string) {
    return new Promise(resolve => {
      this.http.get(`${URL}=${ncuenta}`).subscribe(data => {
        resolve(data);
          return data;
      });
    });
  }
  recuperarCreditos(ncuenta: string) {
    this.creditos = new Array<Credito>();
    this.http.get(`${RecuperCuentaService.URL2}=${ncuenta}`).subscribe((data) => {
      console.log(data);
        for (const index in data){
          let credito = new Credito();
          let datos = data[index];
          credito.codigo=datos['codigo'];
          credito.monto=datos['monto'];
          credito.saldo=datos['saldo'];
          this.creditos.push(credito);
        }
      });
      return this.creditos;
  }
  recuperarPagos(ncuenta: string) {
    this.pagos = new Array<Pago>();
    this.http.get(`${RecuperCuentaService.URL3}=${ncuenta}`).subscribe((data) => {
      console.log(data);
        for (const index in data){
          let pago = new Pago();
          let datos = data[index];
          pago.numeroPago=datos['numeroPago'];
          pago.fechaPago=datos['fechaPago'];
          pago.estado=datos['estado'];
          pago.saldo=datos['saldo'];
          
          this.pagos.push(pago);
        }
      });
      return this.pagos;
  }

}
