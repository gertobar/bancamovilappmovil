import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

const URL = "http://localhost:8080/ProyectoAppDisServer/ws/login";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private storage: Storage, private router: Router) { }


  session(email: String, pass: String) {
    var user = { email: email, clave: pass };
    return new Promise(resolve => {
      this.http.post(URL, user).subscribe(data => {
        resolve(data);
        console.log(data);
        let decoded = this.parseJwt(data['jwt']);
        this.getToken();
        if (this.saveToken) {
          this.router.navigate(['/transaccion', decoded['nmeroCuenta']])
        }
      }, err => {
        console.log(err);
      });
    });

  }

  parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

  saveToken(token: String) {
    this.storage.set('token', token);
  }

  async getToken() {
    var response;
    await this.storage.get('token').then((val) => response = val);
    console.log(this.parseJwt(response));
    return response;
  }
}
